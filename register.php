<section class="envor-section dr submit register">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2><center> نحوه  <strong>ارسال مقاله</strong>  </center></h2>
                <div class="envor-relative pato20 dr tari" id="submit">

                    <p><strong>برای ارسال مقاله باید فایل ODT و PDF را در <a href="">بخش کاربری</a> آپلود کنید. به این نکات توجه کنید که:</strong></p>

                    <ul>
                        <li>مقاله حتما باید در حوزه‌ی نرم افزار آزاد/متن باز باشد.</li>
                        <li>قبلا در کنفرانس یا همایشی ارائه نشده باشد.</li>
                        <li>شیوه‌ی نگارش مقاله بسیار ساده است. از <a href="sample.odt">اینجا</a> دانلود کنید.</li>
                        <li>فونت فایل نمونه Droid Arabic Naskh است. شما می توانید این فونت را <a href="http://fonts.gstatic.com/ea/droidarabicnaskh/v7/download.zip">دانلود</a> کنید یا هر فونتی که می‌خواهید انتخاب کنید و اندازه ی آن را متناسب با فایل مورد نظر تنظیم کنید.</li>
                        <li>ابتدا چکیده را ارسال کنید و پس از پذیرفته شدن چکیده ها مقاله خود را به صورت کامل ارسال کنید.</li>
                    </ul>



                </div>

                <div class="alert alert-danger dr"><center>آخرین مهلت ارسال <strong>چکیده</strong> تا تاریخ <strong>۱۰ مرداد</strong> و آخرین مهلت ارسال <strong>مقاله</strong> تا تاریخ <strong>۱۰ شهریور</strong> ماه است.</center></div>
                <div class="clear"></div>

            </div>
        </div>
    </div>
</section>



<section data-stellar-background-ratio="0.5" class="envor-section envor-section-align-center envor-section-bg2">
    <h2><strong>اطلاعات کلی</strong> </h2>
    <div class="clear mato20"></div>
    <div class="container">
        <div style="margin-top: 0px;" class="row">

            <div style="margin-top: 0px;"  id="info"  class="col-lg-3 col-md-3 col-sm-6">
                <article class="envor-feature-3">
                    <span><i class="glyphicon glyphicon-calendar"></i></span>
                    <h3 style="margin-top: 0px;"><b>زمان برگزاری</b></h3>
                    <p>این همایش به مدت دو روز در روزهای ۲۶ و ۲۷ شهریورماه (پنج&zwnj;شنبه و جمعه) برگزار می‌شود. همایش از ساعت ۹ صبح روز ۲۶ شهریور آغاز می‌شود و تا ساعت ۴ بعد از ظهر روز ۲۷ شهریور ادامه دارد.</p>
                </article>
            </div>

            <div style="margin-top: 0px;" class="col-lg-3 col-md-3 col-sm-6">
                <article class="envor-feature-3">
                    <span><i class="glyphicon glyphicon-time"></i></span>
                    <h3 style="margin-top: 0px;"><b>زمان بندی کلی روز اول</b></h3>
                    <p>ساعت ۹ برنامه آغاز می‌شود. ساعت ۱۱ استراحت. ساعت ۱ تا ۲ ناهار. ساعت ۴ استراحت. ساعت ۷ پایان روز اول. زنجانگردی و سپس صرف شام.</p>
                </article>
            </div>

            <div style="margin-top: 0px;" class="col-lg-3 col-md-3 col-sm-6">
                <article class="envor-feature-3">
                    <span><i class="glyphicon glyphicon-time"></i></span>
                    <h3 style="margin-top: 0px;"><b>زمان بندی کلی روز دوم</b></h3>
                    <p>ساعت ۹ برنامه آغاز می‌شود. ساعت ۱۱ استراحت. ساعت ۱ تا ۲ ناهار. ساعت ۴ استراحت. اختتامیه. سپس در صورت تمایل دوستان می توانند در تور تفریحی که از یکی از بناهای تاریخی بازدید خواهد شد شرکت کنند.</p>
                </article>
            </div>

            <div style="margin-top: 0px;" class="col-lg-3 col-md-3 col-sm-6">
                <article class="envor-feature-3">
                    <span><i class="glyphicon glyphicon-list"></i></span>
                    <h3 style="margin-top: 0px;"><b>محورهای همایش</b></h3>
                    <p>هر مبحث یا تکنولوژی که با مجوز آزاد/متن باز باشد می تواند از محور های این همایش باشد. اگر در هر یک از این محور ها تخصصی دارید یا علاقه مند به آشنایی هستید می توانید در این همایش شرکت کنید.</p>
                </article>
            </div>

            <div style="margin-top: 0px;" class="col-lg-3 col-md-3 col-sm-6">
                <article class="envor-feature-3">
                    <span><i class="glyphicon glyphicon-list-alt"></i></span>
                    <h3 style="margin-top: 0px;"><b>شرایط شرکت</b></h3>
                    <p>در هر سن و سالی که باشید با هر تفکری می توانید به راحتی در این همایش شرکت کنید. هیچ محدودیتی وجود ندارد. برای حضور در همایش فقط دو نکته را در نظر داشته باشید: لبخند بزنید و لباس گرم را فراموش نکنید <i class="fa fa-smile-o"></i></p>
                </article>
            </div>

            <div style="margin-top: 0px;" class="col-lg-3 col-md-3 col-sm-6">
                <article class="envor-feature-3">
                    <span><i class="glyphicon glyphicon-map-marker"></i></span>
                    <h3 style="margin-top: 0px;"><b>محل برگزاری</b></h3>
                    <p>زنجان . انتهای خیابان فاتح . خیابان پروین اعتصامی . جنب شرکت شیرپاستوریزه&zwnj;ی پگاه زنجان . کانون فرهنگی شیخ الاشراق سهروردی</p>
                </article>
            </div>

            <div style="margin-top: 0px;" class="col-lg-3 col-md-3 col-sm-6">
                <article class="envor-feature-3">
                    <span><i class="glyphicon glyphicon-link"></i></span>
                    <h3 style="margin-top: 0px;"><b>نحوه ثبت نام</b></h3>
                    <p>برای ثبت نام می توانید به نشانی <a href="http://users.zconf.ir">users.zconf.ir</a> بروید و به راحتی ثبت نام کنید. فقط عجله کنید. اگر دیرتر از موعد مقرر ثبت نام کنید هزینه ها دوبرابر می‌شوند <i class="fa fa-smile-o"></i></p>
                </article>
            </div>

            <div style="margin-top: 0px;" class="col-lg-3 col-md-3 col-sm-6">
                <article style="background:#341910;color: #fff;" class="envor-feature-3">
                    <span><i class="glyphicon glyphicon-bell"></i></span>
                    <h3 style="margin-top: 0px;color:#fff;"><b>زمان ثبت نام</b></h3>
                    <p>اگر تا تاریخ ۱ مرداد ثبت نام کنید هزینه شرکت در همایش <b style="color: #f49ac1;">۸۰ هزار تومن</b> خواهد بود. تا روز همایش هزینه ثبت نام <b style="color: #f49ac1;">۱۰۰ هزار تومن</b>. خانم ها برای ثبت نام پیشاپیش باید هماهنگ کنند (به دلیل مشکلات تامین خوابگاه خواهران)  <i class="fa fa-smile-o"></i></p>
                </article>
            </div>

        </div>          </div>
    </section>




    <section class="envor-section dr register">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                    <h2 class="mato50"><center>نحوه<strong>  شرکت در همایش</strong> </center></h2>

                    <div class="dr tari">

                        <ul>
                            <li>برای شرکت در همایش باید در <a href="">بخش کاربری</a> ثبت نام کنید و ایمیل و اطلاعات واقعی خود را ارسال کنید و با درگاه پرداخت آنلاین هزینه را پرداخت کنید.</li>
                            <li class="red"><strong>از نوشتن نام کاربری به جای نام و نام خانوادگی واقعی خود جدا خودداری کنید. ثبت نام شما در زنجان فقط با کارت ملی یا شناسنامه امکان پذیر است.</strong></li>
                            <li>ثبت نام برای همه الزامی است.</li>
                            <li>هزینه ثبت‌نام در همایش تا <strong>اول مرداد</strong> با تخفیف <strong>۸۰ هزار تومان</strong> می‌باشد.</li>
                            <li>اگر تا اول مرداد ماه ثبت نام نکنید <strong>تا اول شهریور</strong> فرصت دارید که با مبلغ <strong>۱۰۰ هزار تومان</strong> ثبت نام کنید. </li>
                            <li>یک هفته مانده به همایش هزینه ثبت نام <strong>۲۵۰ هزار تومان</strong> خواهد بود.</li>
                            <li>کسانی که مقاله ارسال کنند و پس از بررسی مقاله آنها پذیرفته شود (چه ارائه بشود چه نشود) تیم اجرایی هزینه ثبت نام آنها را پس از حضور در همایش به آنها باز خواهد گرداند.</li>
                            <li>تیم اجرایی هزینه رفت و آمد شما به زنجان را تقبل <strong class='red'>نمی‌کند</strong>!</li>
                            <li>محل اسکان و غذا و پذیرایی به عهده تیم اجرایی است. </li>
                            <li>خانم ها و آقایان همگی در خوابگاه اسکان داده می‌شوند.</li>
                            <li>برای اقامت به صورت متاهلی با اقای هنرمند(09125423133) تماس بگیرید. <strong>تیم اجرایی زیکانف مسئولیتی در قبال این مورد به عهده نمی‌گیرد و آقای هنرمند به صورت شخصی هماهنگی های مورد نیاز را انجام خواهد داد.</strong></li>
                            <li>ایاب و ذهاب از محل همایش تا خوابگاه و برعکس به عهده تیم اجرایی ست. </li>
                            <li>هزینه ثبت نام در صورت عدم شرکت در همایش قابل بازگشت نیست.</li>
                            <li>لباس گرم فراموش نشود <i class="fa fa-smile-o"></i></li>
                        </ul>
                        <br>
                        <br>

                        اگر سوالی داشتید می تونید با ایمیل <img src="img/email.png" alt="zconf email"> تماس بگیرید یا به شماره تلفن هایی که داده شده زنگ بزنید. 

                    </div>
                </div>
            </div>
        </div>
    </section>

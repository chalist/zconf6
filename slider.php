<section class="envor-section envor-home-slider">
    <div id="layerslider" class="envor-layerslider" style="height: 500px;">
        <div class="ls-layer" style="transition2d: 2,8,30;">
            <img class="ls-bg" src="./img/slides/1.jpg" alt="layer1-background">
            <div class="envor-layerslider-block ls-s1" style="top: 300px; left: 15px; transition2d: all; slidedelay: 6000; durationin: 1000; easingin: easeOutExpo;">
                <h3>همایش سراسری نرم افزارهای</h3>
                <h2> آزاد/متن‌باز</h2>
                <p>فرصت خوبی برای ماست تا از دانسته های هم استفاده کنیم و در کنار هم با جدیدترین تکنولوژی های این حوزه آشنا شویم</p>
            </div>
        </div>
        <div class="ls-layer" style="transition2d: 4,5,23;">
            <img class="ls-bg" src="./img/slides/2.jpg" alt="layer1-background">
            <div class="envor-layerslider-block ls-s1" style="top: 300px; left: 15px; transition2d: all; slidedelay: 6000; durationin: 1000; easingin: easeOutExpo;">
                <h3>در این همایش</h3>
                <h2>دو روزه</h2>
                <p>می توانیم ارتباطات خود را گسترش دهیم و با کسانی که مثل ما در نقاط دیگر این کشور حامی آزادی نرم‌افزار هستند ارتباط بگیریم و کسب تجربه کنیم </p>
            </div>
        </div>
        <div class="ls-layer" style="transition2d: 4,5,23;">
            <img class="ls-bg" src="./img/slides/3.jpg" alt="layer1-background">
            <div class="envor-layerslider-block ls-s1" style="top: 300px; left: 15px; transition2d: all; slidedelay: 6000; durationin: 1000; easingin: easeOutExpo;">
                <h3>در زیکانف می توانیم</h3>
                <h2>بزرگ فکر کنیم</h2>
                <p>ارتباط با بزرگان این عرصه به ما هم کمک می‌کند تا بزرگ شویم و بزرگتر فکر کنیم. می‌توانیم به صورت رایگان از تجربیاتشان استفاده کنیم و از حضور در این جمع لذت ببریم :)</p>
            </div>
        </div>
    </div>

</section>

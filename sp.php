<section class="envor-section envor-section-align-center envor-section-bg2" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2>حامیان <strong>ویژه</strong></h2>
                <div class="envor-relative" id="sponsors">
                    <div class="col-md-3">
                        <div style="" class="inner sp-logo">
                            <a href="https://zanjan.ict.gov.ir/"><img alt="اداره کل ارتباطات و فناوری اطلاعات استان زنجان" src="img/sp/ict.png"></a>
                        </div>
                        <a href="https://zanjan.ict.gov.ir/">اداره کل ارتباطات و فناوری اطلاعات استان زنجان</a>
                    </div>
                    <div class="col-md-3">
                        <div style="" class="inner sp-logo">
                            <a href="http://zanjanlug.org/"><img alt="گروه کاربران لینوکس زنجان" src="img/sp/zlug.png"></a>
                        </div>
                        <a href="http://zanjanlug.org/">گروه کاربران لینوکس زنجان</a>
                    </div>
                    <div class="col-md-3">
                        <div style="" class="inner sp-logo">
                            <a href="http://rokhgroup.com/" class="mabo10"><img alt="گروه نرم افزاری رخ" src="img/sp/rokhgroup.png"></a>
                            <a href="http://rokhgroup.com/">گروه نرم افزاری رخ</a>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div style="" class="inner sp-logo">
                            <a href="http://datispars.com/" class="mabo10"><img style="height:180px;" alt="داتیس" src="img/sp/datis.png"></a>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            
            <!-- 
            <hr>
            <div class="col-lg-12">
                <h2>حامیان <strong>2nate.com</strong></h2>
                <center>
                    <div class="2nate-users avatars" id="sponsors">
                        <?php 
                        // $url='https://2nate.com/json/zconf.php';
                        // $str=file_get_contents($url);
                        // $users = json_decode($str);
                        // foreach ($users as $user) {
                        //     echo '<div class="avatar"><img src="'.$user->avatar.'" data-toggle="tooltip" data-placement="bottom" title="'.$user->name.'" alt="'.$user->name.'"> </div>';
                        // }
                        ?>
                    </div>
                </center>
            </div>
        -->
    </div>
</div>
</section>


<section class="envor-section envor-section-align-center">

    <div class="col-lg-12">
        <h2>حامیان </h2>
        <div class="envor-relative" id="sponsors">
            <center>
                <div style="width: 100px; display: inline-block;margin-left:10px;" class="inner sp-logo">
                    <a href="http://fundly.ir/projects/zconf6"><img class="img-rounded" alt="fundly.ir" src="img/sp/fundly-s.png"></a>
                </div>
            </center>
        </div>
    </div>
</section>

<hr>

<section class="envor-section envor-section-align-center">

    <div class="col-lg-12">
        <h2>حامیان <strong>رسانه‌ای</strong></h2>
        <div class="envor-relative" id="sponsors">
            <center>
                <div style="width: 150px; display: inline-block;margin-left:10px;" class="inner sp-logo">
                    <a href="https://2nate.com/api/zconf"><img class="img-rounded" alt="2nate.com" src="img/sp/2nate.gif"></a>
                </div>
                <div style="width: 150px; display: inline-block;margin-left:10px;" class="inner sp-logo">
                    <a href="https://salam-donya.ir/"><img class="img-rounded" alt="salam-donya.ir" src="img/sp/salam.png"></a>
                </div>
                <div style="height: 54px; display: inline-block;margin-left:10px;" class="inner sp-logo">
                    <a href="http://www.sokanacademy.com/"><img height="54" class="img-rounded" alt="salam-donya.ir" src="img/sp/sokan.png"></a>
                </div>
            </center>
        </div>
    </div>
</section>
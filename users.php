<section class="envor-section dr submit register">
    <h2><center>  <strong>شرکت کنندگان </strong> این دوره </center></h2>

    <?php 
    function CallAPI($method, $url, $data = false)
    {
        $curl = curl_init();

        switch ($method)
        {
            case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);

            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            break;
            case "PUT":
            curl_setopt($curl, CURLOPT_PUT, 1);
            break;
            default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }

    $users = CallAPI('GET', 'http://users.zconf.ir/api/v1/conference/user/');
    $us = json_decode($users);
    ?>
    <div class="avatars">
        <?php
        foreach ($us->objects as $user) {
            ?>
            <div class="avatar">
                <img src="<?php echo $user->avatar; ?>?s=64&d=monsterid" data-toggle="tooltip" data-placement="bottom" title="<?php echo $user->first_name.' '.$user->last_name; ?>" alt="<?php echo $user->first_name.' '.$user->last_name; ?>">
            </div>
            <?php
        }

        ?>

        <p><strong>اگر شما هم می خواهید در این همایش شرکت کنید می توانید از <a href="http://users.zconf.ir/">بخش کاربران</a> اقدام کنید!</strong></p>
    </div>

    <hr class="mato50">
    <h2><center>   <strong>حامیان مالی </strong> </center></h2>
    <?php 
    $donate = CallAPI('GET', 'http://users.zconf.ir/api/v1/conference/donate/');
    $donaters = json_decode($donate);

    ?>
    <div class="avatars">
        <?php
        if (!$donaters->objects) {
            ?>
            <p>تا به حال کسی حمایت نکرده است. شما می توانید اولین حامی مالی باشید. وارد <a href="http://users.zconf.ir/">بخش کاربران</a> سایت شوید!</p>
            <p>اگر نمی خواهید در سایت ثبت نام کنید می توانید مبلغ مورد نظر خود را به شماره حساب <strong>6219861005991418</strong> در بانک سامان به نام محمد نبی زاده واریز کنید.</p>
            <?php
        }
        else{

            foreach ($donaters->objects as $d) {
                ?>
                <div class="avatar">
                    <img src="<?php echo $d->avatar; ?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo $d->first_name.' '.$d->last_name; ?>" alt="<?php echo $d->first_name.' '.$d->last_name; ?>" />
                </div>
                <?php
            }

            $users_f = CallAPI('GET', 'http://fundly.ir/reports/zconf6.php');
            $users_fundly = json_decode($users_f);
            foreach ($users_fundly as $uf) {
                ?>
                <div class="avatar">
                    <img src="<?php echo $uf->image; ?>?s=64&d=monsterid" data-toggle="tooltip" data-placement="bottom" title="<?php echo $uf->name; ?>" alt="<?php echo $uf->name; ?>" style="width: 64px;" />
                </div>
                <?php
            }
            ?>
            <p><strong>شما هم می توانید از ما حمایت کنید. وارد <a href="http://users.zconf.ir/">بخش کاربران</a> سایت شوید!</strong></p>
            <p>اگر نمی خواهید در سایت ثبت نام کنید می توانید مبلغ مورد نظر خود را به شماره حساب <strong>6219861005991418</strong> در بانک سامان به نام محمد نبی زاده واریز کنید.</p>
            <p>
                علاوه بر این می توانید از طریق سایت <a href="http://fundly.ir/projects/zconf6">fundly.ir</a> یا <a href="https://2nate.com/api/zconf">2nate.com</a> نیز اقدام به کمک کنید.
            </p>

            <?php 
        }
        ?>

    </div>
</section>
<footer class="envor-footer envor-footer-1">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6" id="contactus">
                <div class="envor-widget">
                    <h3>شما هم می توانید حامی ما باشید</h3>
                    <div class="envor-widget-inner">
                        <p>برای دریافت اطلاعات مربوط به حامیان با ما تماس بگیرید</p>
                        <span id="subscribe_response_div"></span>
                    </div>
                </div>
                <div class="envor-widget">
                    <h3>با هر توانی می توانید کمک کنید!</h3>
                    <div class="envor-widget-inner">
                        <p>اگر توانایی انجام کاری را دارید می توانید در <a href="https://trello.com/b/sKo5mbun"> بورد عمومی زیکانف ۶ </a> عضو شوید و اعلام همکاری کنید.
                            <br> اگر می خواهید به اندازه‌ی توانتان سهمی در برگزاری هر چه بهتر این همایش داشته باشید می توانید هر مبلغی که مایلید به همایش هدیه دهید :) </p>
                            <p>مبالغ مورد نظر خود را به شماره <strong>6219861005991418</strong> به نام <strong>محمد نبی‌زاده</strong> منتقل کنید</p>
                            <span id="subscribe_response_div"></span>
                        </div>
                        <a href="img/map-img.png"><img src="img/map-img.png" width="100%" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="envor-widget envor-contacts-widget">
                        <h3>تماس با ما</h3>
                        <div class="envor-widget-inner">
                            <p><strong>محل دبیرخانه</strong><br>
                                <i class="fa fa-map-marker"></i> تهران . خیابان انقلاب . پل چوبی به سمت میدان امام حسین . کوچه گرامی . پلاک ۴
                            </p>
                            <p> <strong>محل برگزاری</strong> <br>
                                <i class="fa fa-map-marker"></i> زنجان . انتهای خیابان فاتح . خیابان پروین اعتصامی . جنب شرکت شیرپاستوریزه‌ی پگاه زنجان . کانون فرهنگی شیخ الاشراق سهروردی
                                <center>
                                    <br>
                                    <br>
                                    <a href="https://goo.gl/maps/NcTtI">نقشه در گوگل</a>
                                    <br><br><img src="img/map.png" alt="">
                                </center>
                            </p>
                            <p>
                                <i class="fa fa-phone"></i> تلفن دبیرخانه: <strong>۷۷۶۵۲۱۶۲</strong>
                                <br> محمد نبی‌زاده: <strong>۹۱۲۲۴۲۹۲۹۷</strong>
                            </p>
                            <p>
                                <i class="fa fa-envelope"></i>
                                <a href="mailto:info@zconf">info@zconf.ir</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
</div>
</div>
<?php include_once 'script.php'; ?>
</body>

</html>

<!--[if lt IE 7]>
<p class="chromeframe">برای دیدن سایت از فایرفاکس یا کروم استفاده کنید.</p>
<![endif]-->
<div id="to-the-top"><i class="fa fa-chevron-up"></i></div>
<div id="envor-preload">
	<br>
	<br>
	<div class="clear"></div>
	<br>
	<br>
	<div class="clear"></div>
	<img src="img/l.png" alt="">
	<br>
	<br>
	<div class="clear"></div>
	<span>درحال بارگیری<br>شکیبا باشید</span>
	<p></p>
</div>


<i class="glyphicon glyphicon-align-justify" id="envor-mobile-menu-btn"></i>
<div class="envor-mobile-menu" id="envor-mobile-menu">
	<h3>منو</h3>
	<nav>
		<?php include 'menu.php'; ?>
	</nav>
</div>
<header class="envor-header envor-header-1">
	<div class="envor-top-bar">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<p class="contacts"><i class="fa fa-phone"></i> 912-242-9297</p>
					<p class="contacts"><i class="fa fa-envelope"></i> info @ zconf . ir</p>
					<ul class="social-btns">
						<li><a href="https://www.facebook.com/zconf"><i class="fa fa-facebook"></i></a></li>
						<li><a href="https://twitter.com/zconf"><i class="fa fa-twitter"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="envor-header-bg" id="envor-header-menu">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="envor-relative">

						<a href="http://zconf.ir">
							<div class="envor-logo">
								<img src="./img/l.png" alt="ZC6NF">
							</div>
						</a>

						<nav>
							<?php include 'menu.php'; ?>
						</nav>

					</div>
				</div>
			</div>
		</div>
	</div>

</header>
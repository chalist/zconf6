<section class="envor-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <center>
                    <h2> <strong>داوران </strong> این دوره </h2>
                </center>
                <div class="envor-relative" id="viewers">
                    <div class="envor-project col-md-3">
                        <div class="envor-project-inner">
                            <figure>
                                <img src="./img/viewer/samir.jpg" alt="">
                            </figure>
                            <div class="envor-project-details">
                                <h3 class="link"><strong>سمیر رحمانی</strong></h3>
                                <p class="">برنامه نویس روبی، از اعضای فعال کرج لاگ که سالهاست در زمینه نرم افزارهای ازاد/متن باز فعال هستم.</p>
                            </div>
                        </div>
                    </div>
                    <div class="envor-project col-md-3">
                        <div class="envor-project-inner">
                            <figure>
                                <img src="./img/viewer/mahdiye.jpg" alt="">
                            </figure>
                            <div class="envor-project-details">
                                <h3 class="link"><strong>مهدیه سعید</strong></h3>
                                <p class="">توسعه دهنده نرم افزار <br>(Core Developer)،<br> طراح  پایگاه های داده ای رابطه ای <br>و متخصص VOIP</p>
                            </div>
                        </div>
                    </div>

                    <div class="envor-project col-md-3">
                        <div class="envor-project-inner">
                            <figure>
                                <img src="./img/viewer/peyman.jpg" alt="">
                            </figure>
                            <div class="envor-project-details">
                                <h3 class="link"><strong>پیمان کریمی</strong></h3>
                                <p class="">علاقمند به نرم‌افزار آزاد و آزادی نرم‌افزار<br>
                                    توسعه‌دهنده و مشاور براساس ابزار آزاد و متن‌باز</p>
                                </div>
                            </div>
                        </div>

                        <div class="envor-project col-md-3">
                            <div class="envor-project-inner">
                                <figure>
                                    <img src="./img/viewer/vahid.jpg" alt="">
                                </figure>
                                <div class="envor-project-details">
                                    <h3 class="link"><strong>وحید نگاهداری</strong></h3>
                                    <p class="">توسعه سیستم‌های نرم‌افزاری در حوزه Telecom با زبان Python. علاقمند به مباحث Distributed Computing و Integrated Engineering
                                    </p>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </section>

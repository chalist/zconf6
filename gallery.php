<section class="envor-section envor-section-align-center envor-section-bg2" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2><strong>تصاویر دوره قبل</strong></h2>
                <div class="envor-wrapper" id="gallery">

                    <ul>
                        <li><a  class="fancybox-thumb" rel="fancybox-thumb" title="zconf5 photo gallery" href="img/gallery/01.jpg"><img src="img/gallery/th-01.jpg" alt="zconf5 gallery"></a></li>
                        <li><a  class="fancybox-thumb" rel="fancybox-thumb" title="zconf5 photo gallery" href="img/gallery/02.jpg"><img src="img/gallery/th-02.jpg" alt="zconf5 gallery"></a></li>
                        <li><a  class="fancybox-thumb" rel="fancybox-thumb" title="zconf5 photo gallery" href="img/gallery/04.jpg"><img src="img/gallery/th-04.jpg" alt="zconf5 gallery"></a></li>
                        <li><a  class="fancybox-thumb" rel="fancybox-thumb" title="zconf5 photo gallery" href="img/gallery/05.jpg"><img src="img/gallery/th-05.jpg" alt="zconf5 gallery"></a></li>
                        <li><a  class="fancybox-thumb" rel="fancybox-thumb" title="zconf5 photo gallery" href="img/gallery/06.jpg"><img src="img/gallery/th-06.jpg" alt="zconf5 gallery"></a></li>
                        <li><a  class="fancybox-thumb" rel="fancybox-thumb" title="zconf5 photo gallery" href="img/gallery/07.jpg"><img src="img/gallery/th-07.jpg" alt="zconf5 gallery"></a></li>
                        <li><a  class="fancybox-thumb" rel="fancybox-thumb" title="zconf5 photo gallery" href="img/gallery/09.jpg"><img src="img/gallery/th-09.jpg" alt="zconf5 gallery"></a></li>
                        <li><a  class="fancybox-thumb" rel="fancybox-thumb" title="zconf5 photo gallery" href="img/gallery/10.jpg"><img src="img/gallery/th-10.jpg" alt="zconf5 gallery"></a></li>
                        <li><a  class="fancybox-thumb" rel="fancybox-thumb" title="zconf5 photo gallery" href="img/gallery/11.jpg"><img src="img/gallery/th-11.jpg" alt="zconf5 gallery"></a></li>
                        <li><a  class="fancybox-thumb" rel="fancybox-thumb" title="zconf5 photo gallery" href="img/gallery/12.jpg"><img src="img/gallery/th-12.jpg" alt="zconf5 gallery"></a></li>
                        <li><a  class="fancybox-thumb" rel="fancybox-thumb" title="zconf5 photo gallery" href="img/gallery/13.jpg"><img src="img/gallery/th-13.jpg" alt="zconf5 gallery"></a></li>
                        <li><a  class="fancybox-thumb" rel="fancybox-thumb" title="zconf5 photo gallery" href="img/gallery/14.jpg"><img src="img/gallery/th-14.jpg" alt="zconf5 gallery"></a></li>
                        <li><a  class="fancybox-thumb" rel="fancybox-thumb" title="zconf5 photo gallery" href="img/gallery/15.jpg"><img src="img/gallery/th-15.jpg" alt="zconf5 gallery"></a></li>
                        <li><a  class="fancybox-thumb" rel="fancybox-thumb" title="zconf5 photo gallery" href="img/gallery/16.jpg"><img src="img/gallery/th-16.jpg" alt="zconf5 gallery"></a></li>
                        <li><a  class="fancybox-thumb" rel="fancybox-thumb" title="zconf5 photo gallery" href="img/gallery/17.jpg"><img src="img/gallery/th-17.jpg" alt="zconf5 gallery"></a></li>
                        <li><a  class="fancybox-thumb" rel="fancybox-thumb" title="zconf5 photo gallery" href="img/gallery/18.jpg"><img src="img/gallery/th-18.jpg" alt="zconf5 gallery"></a></li>
                        <li><a  class="fancybox-thumb" rel="fancybox-thumb" title="zconf5 photo gallery" href="img/gallery/19.jpg"><img src="img/gallery/th-19.jpg" alt="zconf5 gallery"></a></li>
                        <li><a  class="fancybox-thumb" rel="fancybox-thumb" title="zconf5 photo gallery" href="img/gallery/20.jpg"><img src="img/gallery/th-20.jpg" alt="zconf5 gallery"></a></li>
                        <li><a  class="fancybox-thumb" rel="fancybox-thumb" title="zconf5 photo gallery" href="img/gallery/21.jpg"><img src="img/gallery/th-21.jpg" alt="zconf5 gallery"></a></li>
                        <li><a  class="fancybox-thumb" rel="fancybox-thumb" title="zconf5 photo gallery" href="img/gallery/22.jpg"><img src="img/gallery/th-22.jpg" alt="zconf5 gallery"></a></li>
                    </ul>

                    
                    <div class="video">
                        <!-- <iframe width="853" height="480" src="https://www.youtube.com/embed/QtVRhqFYErE?rel=0" frameborder="0" allowfullscreen></iframe> -->
                        <iframe style="border: 0;" src="http://www.parsvid.com/embed/PHH3O/?autoplay=false" allowFullScreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" height="360" width="640" ></iframe>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>


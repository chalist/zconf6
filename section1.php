<section class="envor-section">
    <div class="container">
        <div class="row" style="margin-top: 0px;">
            <div class="col-lg-3 col-md-3 col-sm-6" style="margin-top: 0px;">
                <article class="envor-feature">
                    <div class="envor-feature-inner">
                        <header>
                            <i class="fa fa-users"></i> با هم باشیم
                            <span class="arrow-color"></span></header>
                            <p>هر ساله بهترین برنامه نویسان و مدیران سرور از تمام نقاط ایران دور هم جمع می&zwnj;شوند تا دو روز مفید و به یادماندنی را در تقویم خود ثبت کنند. شما هم می&zwnj;توانید در این همایش در کنار ما باشید!</p>
                        </div>
                    </article>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6" style="margin-top: 0px;">
                    <article class="envor-feature">
                        <div class="envor-feature-inner">
                            <header>
                                <i class="fa fa-mortar-board"></i> از هم یاد بگیریم
                                <span class="arrow-color"></span></header>
                                <p>
                                    هدف اصلی ما آموزش نیست، اما وقتی با هم هستیم ناگزیریم از یاد گرفتن. هر کدام از ما در زمینه ای تخصص داریم که می&zwnj;توانیم به اشتراک بگذاریم...
                                </p>
                            </div>
                        </article>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6" style="margin-top: 0px;">
                        <article class="envor-feature">
                            <div class="envor-feature-inner">
                                <header>
                                    <i class="fa fa-smile-o"></i> لبخند بزنیم
                                    <span class="arrow-color"></span></header>
                                    <p>
                                        ما یک جامعه&zwnj;ی بزرگ و شادیم. ما هر سال از همه جای ایران دور هم جمع می&zwnj;شویم که دیداری تازه کنیم. دوستان جدیدی پیدا کنیم و رابطه های کاری خودرا افزایش دهیم...
                                    </p>
                                </div>
                            </article>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6" style="margin-top: 0px;">
                            <article class="envor-feature">
                                <div class="envor-feature-inner">
                                    <header>
                                        <i class="fa fa-line-chart"></i> بی نهایت فکر کنیم
                                        <span class="arrow-color"></span></header>
                                        <p>
                                            مانند دنیای آزاد / متن باز به بینهایت ها فکر کنیم. ما از هر ایده که کارآمد و تازه باشد پشتیبانی می&zwnj;کنیم، فقط یک شرط دارد: ایده&zwnj;ی شما باید در قالب مجوز های آزاد / متن باز باشد...
                                        </p>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                </section>

<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="./js/vendor/jquery-1.11.0.min.js"></script>
<script src="./js/vendor/core-1.0.5.js"></script>
<script src="./js/bootstrap.min.js"></script>
<script src="./js/jquery.mCustomScrollbar.min.js"></script>
<script src="./js/jquery.mousewheel.min.js"></script>
<script src="./js/jquery.colorbox-min.js"></script>
<script src="./js/preloadCssImages.jQuery_v5.js"></script>
<script src="./js/jquery.stellar.min.js"></script>
<script src="./js/masonry.pkgd.min.js"></script>
<script src="./js/imagesloaded.pkgd.min.js"></script>
<script src="./js/layerslider/jquery-easing-1.3.js" type="text/javascript"></script>
<script src="./js/layerslider/jquery-transit-modified.js" type="text/javascript"></script>
<script src="./js/layerslider/layerslider.transitions.js" type="text/javascript"></script>
<script src="./js/layerslider/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
<script src="./js/jquery.rivathemes.js"></script>
<script src="./js/jquery.fancybox.js"></script>
<script type="text/javascript">
$('document').ready(function() {
    var $container = $('body');
    $container.imagesLoaded(function() {
        $('#layerslider').layerSlider({
            skinsPath: './css/layerslider/skins/',
            skin: 'fullwidth',
            thumbnailNavigation: 'hover',
            hoverPrevNext: false,
            responsive: false,
            responsiveUnder: 1170,
            sublayerContainer: 1170
        });
        $('#footer-news').rivaSlider({
            visible: 1,
            selector: 'envor-post-preview'
        });
        $('#envor-preload').hide();
    });
});
</script>
<script src="./js/envor.js"></script>
<script type="text/javascript">
/*
    Windows Phone 8 и Internet Explorer 10
    */
if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
    var msViewportStyle = document.createElement("style")
    msViewportStyle.appendChild(
        document.createTextNode(
            "@-ms-viewport{width:auto!important}"
        )
    )
    document.getElementsByTagName("head")[0].appendChild(msViewportStyle)
}
</script>
<script type="text/javascript">
$(document).ready(function() {
    $(".fancybox-thumb").fancybox({
        prevEffect: 'none',
        nextEffect: 'none',
        helpers: {
            title: {
                type: 'outside'
            },
            thumbs: {
                width: 50,
                height: 50
            }
        }
    });

    $('body').on('click', 'nav ul li a', function(event) {
        event.preventDefault();
        if ($(this).hasClass('link') == false) {

            if ($(this).parent().hasClass('register-btn')) {
            };
            var h = $(this).attr('href');
            h = h.replace('#', '');

            $("html, body").animate({
                scrollTop: $('#' + h).offset().top - 200
            }, 1000);
            
            if (history.pushState) {
                history.pushState(null, null, '#' + h);
            } else {
                location.hash = '#' + h;
            }
        };
    });

    $('body').on('click', 'nav ul li a.link', function(event) {
        event.preventDefault();
        window.location.href = $(this).attr('href');
    });

    var hash = window.location.hash;
    if (hash) {
        $("html, body").animate({
            scrollTop: $(hash).offset().top - 200
        }, 1000);
    };

  $('[data-toggle="tooltip"]').tooltip();
});
</script>

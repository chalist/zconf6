<section class="envor-section envor-section-align-center envor-section-bg2">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <h2><strong>ما کاربران نرم افزارهای آزاد/متن‌باز <span>آدم فضایی </span> نیستیم <i class="fa fa-smile-o"></i></strong></h2>
                <p>هرچند گاهی بین ما هم مثل همه قشرهای جامعه آدم‌هایی پیدا می شوند که خودشان را از بقیه برتر می‌دانند. اما این آدم‌ها همه چیزی که شما از این جامعه کوچک می بینید نیستند.</p>
                <p>ما اعتقاد داریم که همه چیز باید آزاد باشد و رایگان در اختیار همه قرار بگیرد. ما به این فرهنگ اعتقاد داریم.</p>
                <p>به شما پیشنهاد می کنم یک بار حضور در این جمع را امتحان کنید. هم می توانید مقاله ارسال کنید و هم اینکه فقط حضور داشته باشید. اگر سوالی هم داشتید می توانید بپرسید. با کمال میل به شما پاسخ می‌دهیم.</p>
                <p>
                    <a href="http://users.zconf.ir" class="envor-btn envor-btn-primary envor-btn-normal y"><i class="fa fa-check-circle"></i> ثبت‌نام کنید </a>
                    <a href="mailto:info@zconf.ir" class="envor-btn envor-btn-secondary envor-btn-normal y"><i class="fa fa-question-circle"></i> سوالی دارید بپرسید</a>
                </p>
            </div>
        </div>
    </div>
</section>
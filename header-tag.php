<?php 
$v = '0.0.7';
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="ششمین همایش سراسری نرم افزارهای آزاد/متن باز">
    <meta name="author" content="chalist">
    <link rel="shortcut icon" href="favicon.ico?q=1">
    <title>ZCONF6 :: ششمین همایش سراسری نرم افزارهای آزاد/متن باز</title>
    <!-- Bootstrap core CSS -->
    <link href="./css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <!-- Custom CSS -->
    <link href="./css/animate.css" rel="stylesheet" type="text/css">
    <link href="./css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css">
    <link href="./css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="./css/colorbox-skins/4/colorbox.css" type="text/css">
    <link href="./css/main.css?ver=<?php echo $v; ?>" rel="stylesheet" type="text/css">
    <link href="./css/settings.css" rel="stylesheet" type="text/css">
    <link href="./css/header/h1.css" rel="stylesheet" type="text/css">
    <link href="./css/responsive.css" rel="stylesheet" type="text/css">
    <link href="./css/color5.css" rel="stylesheet" type="text/css" id="envor-site-color">
    <link href="./css/rivathemes.css" rel="stylesheet" type="text/css">
    <!-- LayerSlider styles -->
    <link rel="stylesheet" href="./css/layerslider/css/layerslider.css" type="text/css">
    <link rel="stylesheet" href="./css/jquery.fancybox.css" type="text/css">
    <link rel="stylesheet" href="./css/ch.css?ver=<?php echo $v; ?>" type="text/css">
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="./js/vendor/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="./js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>

<body id="top">
    <div class="envor-boxed-bg" style='background-image: url("./img/pat02.png");'>
        <div class="envor-boxed">
            <div class="envor-boxed-wrapper">
                <?php include_once 'header.php'; ?>

<section class="envor-section">
    <div class="container">
        <div class='row'>
            <div class="envor-relative" id="speakers">
                <center>
                    <h2>
                        <strong><span>سخنرانان</span></strong>
                    </h2>
                </center>
                <div class="col-lg-2 col-md-2 col-sm-6" style="margin-top: 0px;">
                    <center>
                        <img class='img-circle' src="img/m/arash.jpg" alt="آرش همت" />
                        <h3>آرش همت</h3>
                        <span>روش صحیح توسعه پایدار وب‎</span>
                    </center>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-6" style="margin-top: 0px;">
                    <center>
                        <img class='img-circle' src="img/m/furud.jpg" alt="فرود غفوری" />
                        <h3>فرود غفوری</h3>
                        <span>Service discovery</span>
                    </center>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-6" style="margin-top: 0px;">
                    <center>
                        <img class='img-circle' src="img/m/behdad.jpg" alt="بهداد اسفهبد" />
                        <h3>بهداد اسفهبد</h3>
                        <span>طراحی و تستِ موتورِ هندیِ حرف‌باز</span>
                    </center>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-6" style="margin-top: 0px;">
                    <center>
                        <img class='img-circle' src="img/m/bardia.jpg" alt="بردیا دانشور" />
                        <h3>بردیا دانشور</h3>
                        <span>QML</span>
                    </center>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-6" style="margin-top: 0px;">
                    <center>
                        <img class='img-circle' src="img/m/saleh.png" alt="صالح سوزنچی" />
                        <h3>صالح سوزنچی</h3>
                        <span>خط فارسی، فاجه ای پنهان در ای تی ایران</span>
                    </center>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-6" style="margin-top: 0px;">
                    <center>
                        <img class='img-circle' src="img/m/danial.jpeg" alt="دانیال نیکنام" />
                        <h3>دانیال نیکنام</h3>
                        <span>Multithreading</span>
                    </center>
                </div>

                <div class="clear"></div>
                <br><br>
                <div class="clear"></div>


                <div class="col-lg-2 col-md-2 col-sm-6" style="margin-top: 0px;">
                    <center style='direction:ltr !important;'>
                        <img class='img-circle' src="img/m/nazanin.jpg" style="width:100%;" alt="نازنین نواب‌زاده" />
                        <h3>نازنین نواب‌زاده</h3>
                        <span style="direction:ltr !important;">How I learned to stop worrying and love "Big Date"</span>
                    </center>
                </div>


                <div class="col-lg-2 col-md-2 col-sm-6" style="margin-top: 0px;">
                    <center style='direction:ltr !important;'>
                        <img class='img-circle' src="img/m/haji.JPG" alt="مجید حاجی‌بابا" />
                        <h3>مجید حاجی‌بابا</h3>
                        <span>Spark - cluster computing framework</span>
                    </center>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-6" style="margin-top: 0px;">
                    <center style='direction:ltr !important;'>
                        <img class='img-circle' src="img/m/reza.jpg" alt="رضا سامعی" />
                        <h3>رضا سامعی</h3>
                        <span>دسترس‌پذیری - Availability</span>
                    </center>
                </div>


                <div class="col-lg-2 col-md-2 col-sm-6" style="margin-top: 0px;">
                    <center style='direction:ltr !important;'>
                        <img class='img-circle' width="100%" src="img/m/haniye.jpeg" alt="هانیه مقصودی" />
                        <h3>هانیه مقصودی</h3>
                        <span style="direction: rtl !important;">Chef معرفی نرم افزار </span>
                    </center>
                </div>


                <div class="col-lg-2 col-md-2 col-sm-6" style="margin-top: 0px;">
                    <center style='direction:ltr !important;'>
                        <img class='img-circle' width="100%" src="img/m/ehsan.jpg" alt="احسان حسینی" />
                        <h3>احسان حسینی</h3>
                        <span>مدیریت پروژه نرم افزاری از راه دور</span>
                    </center>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-6" style="margin-top: 0px;">
                    <center style='direction:ltr !important;'>
                        <img class='img-circle' width="100%" src="img/m/salek.jpg" alt="محسن سالک" />
                        <h3>محسن سالک</h3>
                        <span>مقاله نویسی در ویکی‌پدیا</span>
                    </center>
                </div>
                <div class="clear"></div>
                <br><br>
                <div class="clear"></div>

                <div class="col-lg-2 col-md-2 col-sm-6" style="margin-top: 0px;"></div>


                <div class="col-lg-2 col-md-2 col-sm-6" style="margin-top: 0px;">
                    <center style='direction:ltr !important;'>
                        <img class='img-circle' width="100%" src="img/m/farid.jpg" alt="فرید احمدیان" />
                        <h3>فرید احمدیان</h3>
                        <span>برنامه‌نویسی فتوسنتزی</span>
                    </center>
                </div>


                <div class="col-lg-2 col-md-2 col-sm-6" style="margin-top: 0px;">
                    <center style='direction:ltr !important;'>
                        <img class='img-circle' width="100%" src="img/m/khalili.jpg" alt="محمدرضا خلیلی" />
                        <h3>محمدرضا خلیلی</h3>
                        <span>بلندر در سیستم های کنترلی</span>
                    </center>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-6" style="margin-top: 0px;">
                    <center style='direction:ltr !important;'>
                        <img class='img-circle' width="100%" src="img/m/vahid.jpg" alt="وحید سهرابلو" />
                        <h3>وحید سهرابلو</h3>
                        <span>شروع واقعی</span>
                    </center>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-6" style="margin-top: 0px;">
                    <center style='direction:ltr !important;'>
                        <img class='img-circle' width="100%" src="img/m/behrooz.jpg" alt="بهروز حسن بیگی" />
                        <h3>بهروز حسن بیگی</h3>
                        <span>تاثیر نرم افزار آزاد/متن باز بر فناوری اطلاعات سلامت</span>
                    </center>
                </div>


            </div>

            <div>
                <style type="text/css">
                    table { border-collapse:collapse;
                       border-spacing:0;
                       empty-cells:show;
                   }
                   td, th { vertical-align:top;
                       font-size:10pt;
                       padding: 5px;
                   }
                   h1, h2, h3, h4, h5, h6 { clear:both }
                   ol, ul { margin:0;
                       padding:0;
                   }
                   li { list-style: none;
                       margin:0;
                       padding:0;
                   }
                   li span. { clear: both;
                       line-height:0;
                       width:0;
                       height:0;
                       margin:0;
                       padding:0;
                   }
                   span.footnodeNumber { padding-right:1em;
                   }
                   span.annotation_style_by_filter { font-size:95%;
                       background-color:#fff000;
                       margin:0;
                       border:0;
                       padding:0;
                   }
                   * { margin:0;
                   }
                   .gr1 { font-size:12pt;
                       writing-mode:page;
                   }
                   .P1 { text-align:center ! important;
                   }
                   .ta1 { writing-mode:rl-tb;
                       margin: 40px auto !important;
                   }
                   .ce1 {  background-color:#00cc00;
                       vertical-align:top;
                       text-align:center ! important;
                       font-size:14pt;
                       font-weight:bold;
                   }
                   .ce10 {  vertical-align:middle;
                       text-align:center ! important;
                       font-size:14pt;
                   }
                   .ce11 {  border-style:none;
                       vertical-align:middle;
                       text-align:center ! important;
                       font-size:12pt;
                   }
                   .ce12 {  border-width:0.0133cm;
                       border-style:solid;
                       border-color:#ddd;
                       vertical-align:middle;
                       text-align:center ! important;
                       font-size:12pt;
                   }
                   .ce13 {  border-width:0.0133cm;
                       border-style:solid;
                       border-color:#ddd;
                       vertical-align:middle;
                       text-align:center ! important;
                       font-size:12pt;
                   }
                   .ce14 {  border-style:none;
                       vertical-align:middle;
                       text-align:center ! important;
                       font-size:14pt;
                   }
                   .ce15 {  vertical-align:middle;
                       text-align:center ! important;
                       font-size:14pt;
                   }
                   .ce16 {  border-width:0.0133cm;
                       border-style:solid;
                       border-color:#ddd;
                       vertical-align:middle;
                       writing-mode:rl-tb;
                       font-size:12pt;
                   }
                   .ce17 {  border-width:0.0133cm;
                       border-style:solid;
                       border-color:#ddd;
                       vertical-align:middle;
                   }
                   .ce18 {  background-color:#bdf9cc;
                       border-width:0.0133cm;
                       border-style:solid;
                       border-color:#ddd;
                       vertical-align:middle;
                       text-align:center ! important;
                       font-size:12pt;
                   }
                   .ce19 {  border-width:0.0133cm;
                       border-style:solid;
                       border-color:#ddd;
                       vertical-align:middle;
                       text-align:right ! important;
                       font-size:12pt;
                   }
                   .ce2 {  background-color:#00cc33;
                       vertical-align:top;
                       text-align:center ! important;
                       font-size:14pt;
                       font-weight:bold;
                   }
                   .ce20 {  background-color:#eeeeee;
                       border-width:0.0133cm;
                       border-style:solid;
                       border-color:#ddd;
                       vertical-align:middle;
                       text-align:center ! important;
                       font-size:12pt;
                   }
                   .ce21 {  vertical-align:middle;
                       text-align:center ! important;
                       font-size:12pt;
                   }
                   .ce22 {  border-width:0.0878cm;
                       border-style:solid;
                       border-color:#ddd;
                       vertical-align:top;
                       text-align:center ! important;
                       writing-mode:rl-tb;
                       font-size:12pt;
                   }
                   .ce3 {  vertical-align:top;
                       text-align:center ! important;
                       font-size:14pt;
                       font-weight:bold;
                   }
                   .ce4 {  vertical-align:middle;
                   }
                   .ce5 {  font-size:12pt;
                       font-style:normal;
                       text-shadow:none;
                       text-decoration:none ! important;
                       font-weight:bold;
                   }
                   .ce6 {  vertical-align:middle;
                       font-size:10pt;
                       font-style:normal;
                       text-shadow:none;
                       text-decoration:none ! important;
                   }
                   .ce7 {  border-style:none;
                       vertical-align:middle;
                       text-align:center ! important;
                       font-size:14pt;
                   }
                   .ce8 {  border-width:0.0133cm;
                       border-style:solid;
                       border-color:#ddd;
                       vertical-align:middle;
                       text-align:center ! important;
                       font-size:14pt;
                   }
                   .ce9 {  vertical-align:middle;
                       text-align:center ! important;
                       font-size:14pt;
                   }
                   .co1 { width:33.45pt;
                   }
                   .co10 { width:241.71pt;
                   }
                   .co11 { width:303.96pt;
                   }
                   .co2 { width:106.61pt;
                   }
                   .co3 { width:273.94pt;
                   }
                   .co4 { width:280.2pt;
                   }
                   .co5 { width:130.76pt;
                   }
                   .co6 { width:80.96pt;
                   }
                   .co7 { width:64.01pt;
                   }
                   .co8 { width:55.25pt;
                   }
                   .co9 { width:21pt;
                   }
                   .ro1 { height:21.6pt;
                   }
                   .ro2 { height:48.5pt;
                   }
                   .ro3 { height:40.11pt;
                   }
                   .ro4 { height:26.84pt;
                   }
                   .T1 {  font-size:9pt;
                   }
                   .T2 { font-size:9pt;
                   }
                   .T3 { font-size:12pt;
                   }
                   .T4 { font-size:10pt;
                   }
                   .T5 {  font-size:12pt;
                   }
                   .table td{vertical-align: middle;}
                   .table p{margin: 0;}
               </style>
               <table cellspacing="0" cellpadding="0" border="0" class="ta1 table table-hover">
                <tbody>
                    <tr>
                        <td class="ce7" style="text-align:left;width:55.25pt;border:0; "></td>
                        <td class="ce11" style="text-align:left;width:21pt;border:0; "></td>
                        <td class="ce14" style="text-align:left;width:64.01pt;border:0; "></td>
                        <td class="ce12" style="text-align:left;width:241.71pt; ">
                            <p>پنج شنبه</p>
                        </td>
                        <td class="ce12" style="text-align:left;width:303.96pt; ">
                            <p>جمعه</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="ce8" style="text-align:right; width:55.25pt; ">
                            <p>08:00</p>
                        </td>
                        <td class="ce12" style="text-align:left;width:21pt; ">
                            <p>تا</p>
                        </td>
                        <td class="ce8" style="text-align:right; width:64.01pt; ">
                            <p>08:30</p>
                        </td>
                        <td class="ce12" style="text-align:left;width:241.71pt; " rowspan="2">
                            <p>ثبت نام</p>
                        </td>
                        <td class="ce12" style="text-align:left;width:303.96pt; ">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="ce8" style="text-align:right; width:55.25pt; ">
                            <p>08:30</p>
                        </td>
                        <td class="ce12" style="text-align:left;width:21pt; ">
                            <p>تا</p>
                        </td>
                        <td class="ce8" style="text-align:right; width:64.01pt; ">
                            <p>09:00</p>
                        </td>
                        <td class="ce12" style="text-align:left;width:303.96pt; " rowspan="2">
                            <p>صبحانه</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="ce8" style="text-align:right; width:55.25pt; ">
                            <p>09:00</p>
                        </td>
                        <td class="ce13" style="text-align:left;width:21pt; ">
                            <p>تا</p>
                        </td>
                        <td class="ce8" style="text-align:right; width:64.01pt; ">
                            <p>09:30</p>
                        </td>
                        <td class="ce12" style="text-align:left;width:241.71pt; " rowspan="2">
                            <p>صبحانه</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="ce8" style="text-align:right; width:55.25pt; ">
                            <p>09:30</p>
                        </td>
                        <td class="ce13" style="text-align:left;width:21pt; ">
                            <p>تا</p>
                        </td>
                        <td class="ce8" style="text-align:right; width:64.01pt; ">
                            <p>10:00</p>
                        </td>
                        <td class="ce19" style="text-align:left;width:303.96pt; ">
                            <p>تاثیر نرم&zwnj;افزار آزاد/متن&zwnj;باز بر اطلاعات سلامت</p>
                            <p><span class="T2">بهروز  حسن بیگی</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td class="ce8" style="text-align:right; width:55.25pt; ">
                            <p>10:00</p>
                        </td>
                        <td class="ce13" style="text-align:left;width:21pt; ">
                            <p>تا</p>
                        </td>
                        <td class="ce8" style="text-align:right; width:64.01pt; ">
                            <p>10:30</p>
                        </td>
                        <td class="ce12" style="text-align:left;width:241.71pt; ">
                            <p>افتتاحیه</p>
                        </td>
                        <td class="ce19" style="text-align:left;width:303.96pt; ">
                            <p>Multithreading</p>
                            <p><span class="T2">دانیال نیکنام</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td class="ce8" style="text-align:right; width:55.25pt; ">
                            <p>10:30</p>
                        </td>
                        <td class="ce13" style="text-align:left;width:21pt; ">
                            <p>تا</p>
                        </td>
                        <td class="ce8" style="text-align:right; width:64.01pt; ">
                            <p>11:00</p>
                        </td>
                        <td class="ce16" style="text-align:right;width:241.71pt; ">
                            <p>برنامه نویسی فتوسنتزی</p>
                            <p><span class="T1">فرید احمدیان</span></p>
                        </td>
                        <td class="ce19" style="text-align:right;width:303.96pt; ">
                            <p>QML</p>
                            <p><span class="T4">بردیا دانشور</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td class="ce8" style="text-align:right; width:55.25pt; ">
                            <p>11:00</p>
                        </td>
                        <td class="ce13" style="text-align:left;width:21pt; ">
                            <p>تا</p>
                        </td>
                        <td class="ce8" style="text-align:right; width:64.01pt; ">
                            <p>11:30</p>
                        </td>
                        <td class="ce16" style="text-align:right;width:241.71pt; ">
                            <p>معرفی نرم افزار Chef</p>
                            <p><span class="T2">هانیه مقصودی</span></p>
                        </td>
                        <td class="ce18" style="text-align:right;width:303.96pt; ">
                            <p>استراحت و پذیرایی</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="ce8" style="text-align:right; width:55.25pt; ">
                            <p>11:30</p>
                        </td>
                        <td class="ce13" style="text-align:left;width:21pt; ">
                            <p>تا</p>
                        </td>
                        <td class="ce8" style="text-align:right; width:64.01pt; ">
                            <p>12:00</p>
                        </td>
                        <td class="ce17" style="text-align:right;width:241.71pt; ">
                            <p><span class="T3">طراحی و تستِ موتورِ هندیِ حرف&zwnj;باز</span></p>
                            <p><span class="T2">بهداد اسفهبد</span></p>
                        </td>
                        <td class="ce19" style="text-align:left;width:303.96pt; ">
                            <p>مدیریت پروژه نرم افزاری از راه دور</p>
                            <p><span class="T2">احسان حسینی</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td class="ce8" style="text-align:right; width:55.25pt; ">
                            <p>12:00</p>
                        </td>
                        <td class="ce13" style="text-align:left;width:21pt; ">
                            <p>تا</p>
                        </td>
                        <td class="ce8" style="text-align:right; width:64.01pt; ">
                            <p>12:30</p>
                        </td>
                        <td class="ce18" style="text-align:left;width:241.71pt; ">
                            <p>استراحت و پذیرایی</p>
                        </td>
                        <td class="ce19" style="text-align:left;width:303.96pt; ">
                            <p>روش صحیح توسعه پایدار وب&lrm;</p>
                            <p><span class="T2">آرش همت</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td class="ce8" style="text-align:right; width:55.25pt; ">
                            <p>12:30</p>
                        </td>
                        <td class="ce13" style="text-align:left;width:21pt; ">
                            <p>تا</p>
                        </td>
                        <td class="ce8" style="text-align:right; width:64.01pt; ">
                            <p>13:00</p>
                        </td>
                        <td class="ce19" style="text-align:left;width:241.71pt; ">
                            <p>دسترس&zwnj;پذیری - Availability</p>
                            <p><span class="T2">رضا سامعی</span></p>
                        </td>
                        <td class="ce19" style="text-align:left;width:303.96pt; ">
                            <p>How I learned to stop worrying and love "Big Date"</p>
                            <p><span class="T2">نازنین نواب&zwnj;زاده</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td class="ce8" style="text-align:right; width:55.25pt; ">
                            <p>13:00</p>
                        </td>
                        <td class="ce13" style="text-align:left;width:21pt; ">
                            <p>تا</p>
                        </td>
                        <td class="ce8" style="text-align:right; width:64.01pt; ">
                            <p>13:30</p>
                        </td>
                        <td class="ce19" style="text-align:left;width:241.71pt; ">
                            <p>Spark - cluster computing framework</p>
                            <p><span class="T2">مجید حاجی بابا</span></p>
                        </td>
                        <td class="ce20" style="text-align:left;width:303.96pt; " rowspan="2">
                            <p>ناهار</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="ce8" style="text-align:right; width:55.25pt; ">
                            <p>13:30</p>
                        </td>
                        <td class="ce13" style="text-align:left;width:21pt; ">
                            <p>تا</p>
                        </td>
                        <td class="ce8" style="text-align:right; width:64.01pt; ">
                            <p>14:00</p>
                        </td>
                        <td class="ce20" style="text-align:left;width:241.71pt; " rowspan="2">
                            <p>ناهار</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="ce8" style="text-align:right; width:55.25pt; ">
                            <p>14:00</p>
                        </td>
                        <td class="ce13" style="text-align:left;width:21pt; ">
                            <p>تا</p>
                        </td>
                        <td class="ce8" style="text-align:right; width:64.01pt; ">
                            <p>14:30</p>
                        </td>
                        <td class="ce19" style="text-align:left;width:303.96pt; ">
                            <p>شروع واقعی</p>
                            <p><span class="T2">وحید سهرابلو</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td class="ce8" style="text-align:right; width:55.25pt; ">
                            <p>14:30</p>
                        </td>
                        <td class="ce13" style="text-align:left;width:21pt; ">
                            <p>تا</p>
                        </td>
                        <td class="ce8" style="text-align:right; width:64.01pt; ">
                            <p>15:00</p>
                        </td>
                        <td class="ce19" style="text-align:left;width:241.71pt; ">
                            <p>خط فارسی، فاجه ای پنهان در ای تی ایران</p>
                            <p><span class="T2">صالح سوزنچی</span></p>
                        </td>
                        <td class="ce12" style="text-align:left;width:303.96pt; " rowspan="2">
                            <p>بحث آزاد</p>
                            <p><span class="T4">من چقدر باحالم؟</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td class="ce8" style="text-align:right; width:55.25pt; ">
                            <p>15:00</p>
                        </td>
                        <td class="ce13" style="text-align:left;width:21pt; ">
                            <p>تا</p>
                        </td>
                        <td class="ce8" style="text-align:right; width:64.01pt; ">
                            <p>15:30</p>
                        </td>
                        <td class="ce12" style="text-align:left;width:241.71pt; " rowspan="2">
                            <p>بحث آزاد</p>
                            <p><span class="T4">مشارکتِ عمومی در توسعه&zwnj;ی نرم&zwnj;افزارِ آزاد</span></p>
                            <p><span class="T4">در سطح بین المللی</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td class="ce8" style="text-align:right; width:55.25pt; ">
                            <p>15:30</p>
                        </td>
                        <td class="ce13" style="text-align:left;width:21pt; ">
                            <p>تا</p>
                        </td>
                        <td class="ce8" style="text-align:right; width:64.01pt; ">
                            <p>16:00</p>
                        </td>
                        <td class="ce18" style="text-align:left;width:303.96pt; ">
                            <p>استراحت و پذیرایی</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="ce8" style="text-align:right; width:55.25pt; ">
                            <p>16:00</p>
                        </td>
                        <td class="ce13" style="text-align:left;width:21pt; ">
                            <p>تا</p>
                        </td>
                        <td class="ce8" style="text-align:right; width:64.01pt; ">
                            <p>16:30</p>
                        </td>
                        <td class="ce18" style="text-align:left;width:241.71pt; ">
                            <p>استراحت و پذیرایی</p>
                        </td>
                        <td class="ce12" style="text-align:left;width:303.96pt; ">
                            <p>اختتامیه</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="ce8" style="text-align:right; width:55.25pt; ">
                            <p>16:30</p>
                        </td>
                        <td class="ce13" style="text-align:left;width:21pt; ">
                            <p>تا</p>
                        </td>
                        <td class="ce8" style="text-align:right; width:64.01pt; ">
                            <p>17:00</p>
                        </td>
                        <td class="ce19" style="text-align:left;width:241.71pt; ">
                            <p><span class="T5">بلندر در سیستم های کنترلی</span></p>
                            <p><span class="T1">محمدرضا خلیلی</span></p>
                        </td>
                        <td class="ce12" style="text-align:left;width:303.96pt; " rowspan="4">
                            <p>بازدید از گنبد سلطانیه</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="ce8" style="text-align:right; width:55.25pt; ">
                            <p>17:00</p>
                        </td>
                        <td class="ce13" style="text-align:left;width:21pt; ">
                            <p>تا</p>
                        </td>
                        <td class="ce8" style="text-align:right; width:64.01pt; ">
                            <p>17:30</p>
                        </td>
                        <td class="ce19" style="text-align:left;width:241.71pt; ">
                            <p>Service discovery</p>
                            <p><span class="T2">فرود غفوری</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td class="ce8" style="text-align:right; width:55.25pt; ">
                            <p>17:30</p>
                        </td>
                        <td class="ce13" style="text-align:left;width:21pt; ">
                            <p>تا</p>
                        </td>
                        <td class="ce8" style="text-align:right; width:64.01pt; ">
                            <p>18:00</p>
                        </td>
                        <td class="ce19" style="text-align:left;width:241.71pt; ">
                            <p>مقاله نویسی در ویکی&zwnj;پدیا</p>
                            <p><span class="T2">محسن سالک</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td class="ce8" style="text-align:right; width:55.25pt; ">
                            <p>18:00</p>
                        </td>
                        <td class="ce13" style="text-align:left;width:21pt; ">
                            <p>تا</p>
                        </td>
                        <td class="ce8" style="text-align:right; width:64.01pt; ">
                            <p>19:00</p>
                        </td>
                        <td class="ce12" style="text-align:left;width:241.71pt; " rowspan="4">
                            <p>بازدید از مناطق تفریحی + شام</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="ce8" style="text-align:right; width:55.25pt; ">
                            <p>19:00</p>
                        </td>
                        <td class="ce13" style="text-align:left;width:21pt; ">
                            <p>تا</p>
                        </td>
                        <td class="ce8" style="text-align:right; width:64.01pt; ">
                            <p>20:00</p>
                        </td>
                        <td class="ce12" style="text-align:left;width:303.96pt; " rowspan="3">
                            <p>رویدادهای همایش را در شبکه های اجتماعی</p>
                            <p>با دو تگِ #zconf و #zconf6 منتشر یا دنبال کنید</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="ce8" style="text-align:right; width:55.25pt; ">
                            <p>20:00</p>
                        </td>
                        <td class="ce13" style="text-align:left;width:21pt; ">
                            <p>تا</p>
                        </td>
                        <td class="ce8" style="text-align:right; width:64.01pt; ">
                            <p>21:00</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="ce8" style="text-align:right; width:55.25pt; ">
                            <p>21:00</p>
                        </td>
                        <td class="ce13" style="text-align:left;width:21pt; ">
                            <p>تا</p>
                        </td>
                        <td class="ce8" style="text-align:right; width:64.01pt; ">
                            <p>22:00</p>
                        </td>
                    </tr>

                </tbody>
            </table>

        </div>
        <center>
            <h3>برای مشاهده زمان ارائه و سخنرانی ها می توانید <a href="timesheet.pdf"><span style="color:#EE4097;">این فایل</span></a> را دانلود کنید</h3>
        </center>
    </div>
</div>
</section>

<?php include_once 'header-tag.php'; ?>

<div class="envor-content">
    <?php include_once 'slider.php'; ?>
    <?php include_once 'section1.php'; ?>
    <?php include_once 'section2.php'; ?>
    <?php include_once 'speaker.php'; ?>
    <?php //include_once 'viewer.php'; ?>
    
    <?php include_once 'users.php'; ?>
    
    <?php include_once 'sp.php'; ?>
    <?php include_once 'register.php'; ?>
    <?php include_once 'gallery.php'; ?>

    <section class="envor-section envor-section-st2 envor-section-cta2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p><span>از حضور شما در همایش به هر طریقی استقبال می کنیم <i class="fa fa-smile-o"></i></span></p>
                </div>
            </div>
        </div>
    </section>
</div>
<?php include_once 'footer.php'; ?>
